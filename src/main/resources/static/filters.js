$(document).ready(function() {

    //-- Get all terminal names--------------------------------------------------------
                    $.ajax({url: "getterminalnames", success: function(result){
                        $('#selectterm_b').empty();
                        $('#selectterm_b').append("<option value=" + "All" + ">All</option>");
                        for(i=0; i<result.length; i++){
                            var option = '<option value =' +result[i]+ '>' +result[i]+'</option>';
                            $('#selectterm_b').append(option);
                        }
                        }});

    //--- Fill all filter values------------------------------------------------------
                        $('#selectdate_b1').empty();
                        $('#selectdate_b2').empty();
                        $('#selectdate_b1').append("<option selected=" + "true" + "disabled=" + "disabled" + ">DD</option> ");
                        $('#selectdate_b2').append("<option selected=" + "true" + "disabled=" + "disabled" + ">DD</option> ");

                     for(i=1; i<=31; i++){
                            var option = '<option value =' + i + '>' + i + '</option>';
                              $('#selectdate_b1').append(option);
                              $('#selectdate_b2').append(option);
                            }

                    $('#selecthour_b1').empty();
                    $('#selecthour_b2').empty();
                    $('#selecthour_b1').append("<option selected=" + "true" + "disabled=" + "disabled" + ">HH</option> ");
                    $('#selecthour_b2').append("<option selected=" + "true" + "disabled=" + "disabled" + ">HH</option> ");
                    for(i=0; i<24; i++){
                            var option = '<option value =' + i + '>' + i +'</option>';
                        $('#selecthour_b1').append(option);
                        $('#selecthour_b2').append(option);

                      }

                    $('#selectmonth_b1').empty();
                    $('#selectmonth_b2').empty();
                    $('#selectmonth_b1').append(" <option selected=" + "true" + "disabled=" + "disabled" + ">MM</option> ");
                    $('#selectmonth_b2').append(" <option selected=" + "true" + "disabled=" + "disabled" + ">MM</option> ");
                    for(i=1; i<=12; i++){
                            var option = '<option value =' +i+ '>' +i+'</option>';
                      $('#selectmonth_b1').append(option);
                      $('#selectmonth_b2').append(option);

                    }

                    $('#selectyear_b1').empty();
                    $('#selectyear_b2').empty();
                    $('#selectyear_b1').append(" <option selected=" + "true" + "disabled=" + "disabled" + ">YYYY</option> ");
                    $('#selectyear_b2').append(" <option selected=" + "true" + "disabled=" + "disabled" + ">YYYY</option> ");
                    for(i=2019; i<2020; i++){
                         var option = '<option value =' +i+ '>' +i+'</option>';
                         $('#selectyear_b1').append(option);
                         $('#selectyear_b2').append(option);

                    }

    console.log( "ready!" );
});