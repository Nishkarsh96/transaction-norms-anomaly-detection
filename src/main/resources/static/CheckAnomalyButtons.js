//---------------- View this week data--------------------------
function view_week() {
    var currentDate = new Date();
    var week_day = currentDate.getDay();
    var days_prior = (week_day-1);

    var start_date = currentDate;
    start_date.setDate(currentDate.getDate() - days_prior);
    start_date.setHours(0);

    // Json with range of timestamp
    var values={
        terminal_name: "All",
        timestamp1_date: start_date.getDate(),
        timestamp1_month: start_date.getMonth(),
        timestamp1_year: start_date.getFullYear(),
        timestamp1_hour: start_date.getHours(),
        timestamp2_date: currentDate.getDate(),
        timestamp2_month: currentDate.getMonth(),
        timestamp2_year: currentDate.getFullYear(),
        timestamp2_hour: currentDate.getHours(),
        };

    // Fetching Anomaly List
    $.ajax({url: "getlistofanomalies",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(values),
        success: function(result) {
        var list = result.lst;
        $('#listofanomalies').empty();
        for(i=0; i<list.length; i++){
            var li = "<li>" + " | "+ list[i].Name + "  |  " +list[i].StatTime + "  |  " +list[i].Total+ "  |  " +list[i].RespTime + " | " + "</li>" ;
            $('#listofanomalies').append(li);
        }

        // Drawing Piechart
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawPieChart);
        function drawPieChart() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Number'],
                ['Anomalies', result.total_anomalies], ['Normal Datapoints', result.total_normal_datapoints]
            ]);

            var options = {
                backgroundColor: 'white',
                title: 'Anomalies & Normal Datapoints',
                legend: 'none',
                pieSliceText: 'label',
                slices: {  0: {offset: 0.2},
                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }

        // Drawing Zoomable LineGraph
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawZoomChart);

        function drawZoomChart() {

            var data_list = result.linegraph_anomalies;
            var data = new google.visualization.DataTable();

            if(result.data_type == "hourly"){

                data.addColumn('string', 'Hour');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [(data_list[i].hour).toString(), parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }
            else{

                data.addColumn('date', 'Date');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [new Date(parseInt(data_list[i].year), parseInt(data_list[i].month), parseInt(data_list[i].date)),
                        parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }

            var options = {
                title: 'Anomalies',
                hAxis: {
                    label: "Date",
                    format: 'd/M/yy',
                    title: 'Year',
                    titleTextStyle: {
                        color: '#333'
                    },
                    slantedText: true,
                    slantedTextAngle: 80
                },
                vAxis: {
                    minValue: 0
                },
                explorer: {
                    actions: ['dragToZoom', 'rightClickToReset'],
                    axis: 'horizontal',
                    keepInBounds: true,
                    maxZoomIn: 4.0
                },
                colors: ['#D44E41'],
            };

            var chart = new google.visualization.LineChart(document.getElementById('zoom_chart'));
            chart.draw(data, options);

        }

    }
    });

}

// -------------View this month data---------------------------------
function view_month() {
    var currentDate = new Date();
    var start_date = new Date();
    start_date.setHours(0);
    start_date.setDate(1);

    // Json with range of timestamp
    var values={
        terminal_name: "All",
        timestamp1_date: start_date.getDate(),
        timestamp1_month: start_date.getMonth(),
        timestamp1_year: start_date.getFullYear(),
        timestamp1_hour: start_date.getHours(),
        timestamp2_date: currentDate.getDate(),
        timestamp2_month: currentDate.getMonth(),
        timestamp2_year: currentDate.getFullYear(),
        timestamp2_hour: currentDate.getHours(),
        };

    // Fetching Anomaly List
    $.ajax({url: "getlistofanomalies",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(values),
        success: function(result) {
        var list = result.lst;
        $('#listofanomalies').empty();
        for(i=0; i<list.length; i++){
            var li = "<li>" + " | "+ list[i].Name + "  |  " +list[i].StatTime + "  |  " +list[i].Total+ "  |  " +list[i].RespTime + " | " + "</li>" ;
            $('#listofanomalies').append(li);
        }

        // Drawing Piechart
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawPieChart);
        function drawPieChart() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Number'],
                ['Anomalies', result.total_anomalies], ['Normal Datapoints', result.total_normal_datapoints]
            ]);

            var options = {
                backgroundColor: 'white',
                title: 'Anomalies & Normal Datapoints',
                legend: 'none',
                pieSliceText: 'label',
                slices: {  0: {offset: 0.2},
                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }

        // Drawing Zoomable LineGraph
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawZoomChart);

        function drawZoomChart() {

            var data_list = result.linegraph_anomalies;
            var data = new google.visualization.DataTable();

            if(result.data_type == "hourly"){

                data.addColumn('string', 'Hour');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [(data_list[i].hour).toString(), parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }
            else{

                data.addColumn('date', 'Date');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [new Date(parseInt(data_list[i].year), parseInt(data_list[i].month), parseInt(data_list[i].date)),
                        parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }

            var options = {
                title: 'Anomalies',
                hAxis: {
                    label: "Date",
                    format: 'd/M/yy',
                    title: 'Year',
                    titleTextStyle: {
                        color: '#333'
                    },
                    slantedText: true,
                    slantedTextAngle: 80
                },
                vAxis: {
                    minValue: 0
                },
                explorer: {
                    actions: ['dragToZoom', 'rightClickToReset'],
                    axis: 'horizontal',
                    keepInBounds: true,
                    maxZoomIn: 4.0
                },
                colors: ['#D44E41'],
            };

            var chart = new google.visualization.LineChart(document.getElementById('zoom_chart'));
            chart.draw(data, options);

        }

    }
    });

}

// ----------------View this year data---------------------
function view_year() {
    var currentDate = new Date();
    var start_date = new Date();
    start_date.setHours(0);
    start_date.setDate(1);
    start_date.setMonth(0);

    // Json with range of timestamp
    var values={
        terminal_name: "All",
        timestamp1_date: start_date.getDate(),
        timestamp1_month: start_date.getMonth(),
        timestamp1_year: start_date.getFullYear(),
        timestamp1_hour: start_date.getHours(),
        timestamp2_date: currentDate.getDate(),
        timestamp2_month: currentDate.getMonth(),
        timestamp2_year: currentDate.getFullYear(),
        timestamp2_hour: currentDate.getHours(),
        };

    // Fetching Anomaly List
    $.ajax({url: "getlistofanomalies",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(values),
        success: function(result) {
        var list = result.lst;
        $('#listofanomalies').empty();
        for(i=0; i<list.length; i++){
            var li = "<li>" + " | "+ list[i].Name + "  |  " +list[i].StatTime + "  |  " +list[i].Total+ "  |  " +list[i].RespTime + " | " + "</li>" ;
            $('#listofanomalies').append(li);
        }

        // Drawing Piechart
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawPieChart);
        function drawPieChart() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Number'],
                ['Anomalies', result.total_anomalies], ['Normal Datapoints', result.total_normal_datapoints]
            ]);

            var options = {
                backgroundColor: 'white',
                title: 'Anomalies & Normal Datapoints',
                legend: 'none',
                pieSliceText: 'label',
                slices: {  0: {offset: 0.2},
                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }

        // Drawing Zoomable LineGraph
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawZoomChart);

        function drawZoomChart() {

            var data_list = result.linegraph_anomalies;
            var data = new google.visualization.DataTable();

            if(result.data_type == "hourly"){

                data.addColumn('string', 'Hour');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [(data_list[i].hour).toString(), parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }
            else{

                data.addColumn('date', 'Date');
                data.addColumn('number', 'Anomalies');

                for(var i = 0; i < data_list.length; i++){
                    data.addRows([
                        [new Date(parseInt(data_list[i].year), parseInt(data_list[i].month), parseInt(data_list[i].date)),
                        parseInt(data_list[i].num_anomalies)]
                    ]);
                }

            }

            var options = {
                title: 'Anomalies',
                hAxis: {
                    label: "Date",
                    format: 'd/M/yy',
                    title: 'Year',
                    titleTextStyle: {
                        color: '#333'
                    },
                    slantedText: true,
                    slantedTextAngle: 80
                },
                vAxis: {
                    minValue: 0
                },
                explorer: {
                    actions: ['dragToZoom', 'rightClickToReset'],
                    axis: 'horizontal',
                    keepInBounds: true,
                    maxZoomIn: 4.0
                },
                colors: ['#D44E41'],
            };

            var chart = new google.visualization.LineChart(document.getElementById('zoom_chart'));
            chart.draw(data, options);

        }


    }
    });

}