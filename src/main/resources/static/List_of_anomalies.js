function fetch_list_anomalies(){

    var terminal_name = $("#selectterm_b").val();
    var timestamp1_date = $("#selectdate_b1").val();
    var timestamp1_month = $("#selectmonth_b1").val();
    var timestamp1_year = $("#selectyear_b1").val();
    var timestamp1_hour = $("#selecthour_b1").val();
    var timestamp2_date = $("#selectdate_b2").val();
    var timestamp2_month = $("#selectmonth_b2").val();
    var timestamp2_year = $("#selectyear_b2").val();
    var timestamp2_hour = $("#selecthour_b2").val();

    // (YYYY, MM, DD, Hr, Min, Sec)
    var g1 = new Date(parseInt(timestamp1_year), parseInt(timestamp1_month), parseInt(timestamp1_date));
    var g2 = new Date(parseInt(timestamp2_year), parseInt(timestamp2_month), parseInt(timestamp2_date));

    if(g1.getTime() <= g2.getTime()) {

        var values={
                 terminal_name: $("#selectterm_b").val(),
                 timestamp1_date: $("#selectdate_b1").val(),
                 timestamp1_month: $("#selectmonth_b1").val(),
                 timestamp1_year: $("#selectyear_b1").val(),
                 timestamp1_hour: $("#selecthour_b1").val(),
                 timestamp2_date: $("#selectdate_b2").val(),
                 timestamp2_month: $("#selectmonth_b2").val(),
                 timestamp2_year: $("#selectyear_b2").val(),
                 timestamp2_hour: $("#selecthour_b2").val(),
            };

//-- Fetching Anomaly List ---------------------------------------------------------------------------------------------
    $.ajax({
           url: "getlistofanomalies",
           type: "POST",
           dataType: "json",
           contentType: "application/json",
           data: JSON.stringify(values),
           success: function(result) {
                var list = result.lst;
                $('#listofanomalies').empty();
                for(i=0; i<list.length; i++){
                    var li = "<li>" + " | "+ list[i].Name + "  |  " +list[i].StatTime + "  |  " +list[i].Total+ "  |  " +list[i].RespTime + " | " + "</li>" ;
                    $('#listofanomalies').append(li);
                }

//----------------Drawing Piechart------------------------------------------------
                  google.charts.load("current", {packages:["corechart"]});
                  google.charts.setOnLoadCallback(drawPieChart);
                  function drawPieChart() {
                    var data = google.visualization.arrayToDataTable([
                      ['Type', 'Number'],
                      ['Anomalies', result.total_anomalies], ['Normal Datapoints', result.total_normal_datapoints]
                    ]);

                    var options = {
                      backgroundColor: 'white',
                      title: 'Anomalies & Normal Datapoints',
                      legend: 'none',
                      pieSliceText: 'label',
                      slices: {  0: {offset: 0.2},
                      },
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                    chart.draw(data, options);
                  }
//----------------------------------------------------------------------------------------------------------------------

//----------------Drawing Zoomable LineGraph----------------------------------------------------------------------------
      google.charts.load('current', {'packages': ['corechart']});
      google.charts.setOnLoadCallback(drawZoomChart);

      function drawZoomChart() {

      var data_list = result.linegraph_anomalies;
      var data = new google.visualization.DataTable();

      if(result.data_type == "hourly"){

            data.addColumn('string', 'Hour');
            data.addColumn('number', 'Anomalies');

            for(var i = 0; i < data_list.length; i++){
                data.addRows([
                    [(data_list[i].hour).toString(), parseInt(data_list[i].num_anomalies)]
                ]);
            }

      }
      else{

              data.addColumn('date', 'Date');
              data.addColumn('number', 'Anomalies');

              for(var i = 0; i < data_list.length; i++){
                  data.addRows([
                      [new Date(parseInt(data_list[i].year), parseInt(data_list[i].month), parseInt(data_list[i].date)),
                       parseInt(data_list[i].num_anomalies)]
                  ]);
              }

      }

        var options = {
          title: 'Anomalies',
          hAxis: {
          label: "Date",
            format: 'd/M/yy',
            title: 'Year',
            titleTextStyle: {
              color: '#333'
            },
            slantedText: true,
            slantedTextAngle: 80
          },
          vAxis: {
            minValue: 0
          },
          explorer: {
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
            keepInBounds: true,
            maxZoomIn: 4.0
          },
          colors: ['#D44E41'],
        };

        var chart = new google.visualization.LineChart(document.getElementById('zoom_chart'));
        chart.draw(data, options);
      }

//----------------------------------------------------------------------------------------------------------------------

           }
    });

    }

    else{
        alert("Please enter the correct date!");
    }

}
