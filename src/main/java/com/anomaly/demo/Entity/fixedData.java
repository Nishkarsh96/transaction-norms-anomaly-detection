package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class fixedData {
    @JsonProperty("totalnum_anomaliestoday")
    int totalnum_anomaliestoday;
    @JsonProperty("totalterminals")
    int totalterminals;
    @JsonProperty("totalnum_datapointstoday")
    int totalnum_datapointstoday;

    @JsonProperty("todayperhouranomalies")
    int [] todayperhouranomalies = new int[24];
    @JsonProperty("todayperhourdatapoints")
    int [] todayperhourdatapoints = new int[24];

    @JsonProperty("listofanomaliestoday")
    List<DataPoint> listofanomaliestoday;

}
