package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Inputs {
    @JsonProperty("terminal_name")
    String terminal_name;

    @JsonProperty("timestamp1_date")
    int timestamp1_date;
    @JsonProperty("timestamp1_month")
    int timestamp1_month;
    @JsonProperty("timestamp1_year")
    int timestamp1_year;
    @JsonProperty("timestamp1_hour")
    int timestamp1_hour;

    @JsonProperty("timestamp2_date")
    int timestamp2_date;
    @JsonProperty("timestamp2_month")
    int timestamp2_month;
    @JsonProperty("timestamp2_year")
    int timestamp2_year;
    @JsonProperty("timestamp2_hour")
    int timestamp2_hour;
}
