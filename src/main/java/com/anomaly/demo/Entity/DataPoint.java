package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
public class DataPoint {
    @JsonProperty("Name")
    String Name;
    @JsonProperty("StatTime")
    String StatTime;
    @JsonProperty("LastTransaction")
    String LastTransaction;
    @JsonProperty("Total")
    int Total;
    @JsonProperty("Approvals")
    int Approvals;
    @JsonProperty("Denials")
    int Denials;
    @JsonProperty("Reversals")
    int Reversals;
    @JsonProperty("RespTime")
    long RespTime;
    @JsonProperty("TPS")
    double TPS;
}
