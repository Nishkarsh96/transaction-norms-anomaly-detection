package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class line_graph_data {

    @JsonProperty("date")
    int date;
    @JsonProperty("month")
    int month;
    @JsonProperty("year")
    int year;
    @JsonProperty("hour")
    int hour;

    @JsonProperty("num_anomalies")
    int num_anomalies;

}
