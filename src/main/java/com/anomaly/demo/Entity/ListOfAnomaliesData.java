package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ListOfAnomaliesData {
    @JsonProperty("lst")
    List<DataPoint> lst;
    @JsonProperty("total_datapoints")
    int total_datapoints;
    @JsonProperty("total_anomalies")
    int total_anomalies;
    @JsonProperty("total_normal_datapoints")
    int total_normal_datapoints;
//    @JsonProperty("linegraph_datapoints")
//    List<Integer> linegraph_datapoints;
    @JsonProperty("linegraph_anomalies")
    List<line_graph_data> linegraph_anomalies;

    @JsonProperty("data_type")
    String data_type;

}
