package com.anomaly.demo.Entity;

import lombok.Data;

@Data
public class PreprocessedPoint {
    String Name;
    int Total;
    long RespTime;
    int [] Hour = new int[24];
    int [] Weekday = new int[7];
}
