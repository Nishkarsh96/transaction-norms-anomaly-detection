package com.anomaly.demo.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Required_Data {

    @JsonProperty("Name")
    String Name;
    @JsonProperty("Low")
    double Low;
    @JsonProperty("Medium")
    double Medium;
    @JsonProperty("High")
    double High;

}
