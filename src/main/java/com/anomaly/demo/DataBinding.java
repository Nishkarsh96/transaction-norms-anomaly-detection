package com.anomaly.demo;

import com.anomaly.demo.Entity.DataPoint;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface DataBinding {

    String DATA_IN = "datain";
    String DATA_OUT = "dataout";

    @Input(DATA_IN)
    KStream<String, DataPoint> dataIn();

    @Output(DATA_OUT)
    MessageChannel dataOut();
}
