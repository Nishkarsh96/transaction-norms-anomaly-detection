package com.anomaly.demo.ServiceImpl;

import com.anomaly.demo.Entity.DataPoint;
import com.anomaly.demo.Entity.Inputs;
import com.anomaly.demo.Entity.PreprocessedPoint;
import com.anomaly.demo.Entity.line_graph_data;
import com.anomaly.demo.Service.TerminalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hex.genmodel.MojoModel;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.RowData;
import hex.genmodel.easy.prediction.AutoEncoderModelPrediction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import java.awt.*;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class TerminalServiceImpl implements TerminalService {

    @Autowired
    RedisTemplate<String,Object> rt;

    @Override
    public PreprocessedPoint PreprocessData(DataPoint dp) throws ParseException {
        // Setting value of Name Total and RespTime
        PreprocessedPoint ppp = new PreprocessedPoint();
        ppp.setTotal(dp.getTotal());
        ppp.setRespTime(dp.getRespTime());
        ppp.setName(dp.getName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = dateFormat.parse(dp.getStatTime());

        // Onehot Encoding Weekdays feature
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        int [] weekday = new int[7];
        weekday[dayOfWeek-1] = 1;

        // Onehot Encoding Hour feature
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int [] hours = new int[24];
        hours[hour] = 1;

        // Setting the value of weekday and hour
        ppp.setWeekday(weekday);
        ppp.setHour(hours);

        // Returning the object
        return ppp;
    }

    @Override
    public int PredictResults(PreprocessedPoint lpp, DataPoint dp) throws Exception {
        int total = lpp.getTotal();
        String strTotal = Integer.toString(total);

        long resptime = lpp.getRespTime();
        String strResptime = Long.toString(resptime);

        int[] hour = lpp.getHour();
        String strHour[] = Arrays.stream(hour)
                .mapToObj(String::valueOf)
                .toArray(String[]::new);

        int[] weekday = lpp.getWeekday();
        String strWeekday[] = Arrays.stream(weekday)
                .mapToObj(String::valueOf)
                .toArray(String[]::new);

        //Getting the model name
        String nameofmodel = "";
        File folder = new File("/home/nishkarsh/Documents/Anomaly Detection/Models/Model_for_"+lpp.getName());
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles[0].getName().equals("h2o-genmodel.jar")) {
            nameofmodel = listOfFiles[1].getName();
        }
        else{
            nameofmodel = listOfFiles[0].getName();
        }

        // Loading the Mojo Model
        EasyPredictModelWrapper model_orig = new EasyPredictModelWrapper(
                MojoModel.load("/home/nishkarsh/Documents/Anomaly Detection/Models/Model_for_" + lpp.getName() + "/" + nameofmodel));

        // Putting row data
        RowData row = new RowData();

        //Putting total and resptime
        row.put("Total", strTotal);
        row.put("RespTime", strResptime);

        // Putting the Hour feature
        for(int i=0; i<24; i++){
            row.put("['Hour']_"+i, strHour[i]);
        }

        //Putting the weekday feature
        for(int i=1; i<=7; i++){
            row.put("['Weekday']_"+i, strWeekday[i-1]);
        }

        // Predicting Reconstructed Values
        AutoEncoderModelPrediction p = model_orig.predictAutoEncoder(row);

//        double[] reconstructedValues = p.reconstructed;
//        //System.out.println("Reconstructed Values : "+p.reconstructed[0]);
//        double[] originalValues = p.original;
//        //System.out.println("Original Values : "+p.original[0]);
//        double[] squarederror = new double[originalValues.length];
//        double sum = 0.0;
//        // Calculating Reconstruction Error
//        for(int i=0; i<originalValues.length;i++){
//            // Calculate Squared Differences
//            double diff = originalValues[i] - reconstructedValues[i];
//            squarederror[i] = diff * diff;
//            //System.out.println("Reconstructed Values : "+reconstructedValues[i]);
//            //System.out.println("Original Values : "+originalValues[i]);
//        }
//
//        //System.out.println("Squared Error Values : "+squarederror[0]);
//
//        // Adding errors
//        for(int i=0; i<squarederror.length; i++){
//            sum = sum + squarederror[i];
//            //System.out.println("Sum inside loop : "+sum +" : Error : " + squarederror[i]);
//        }
//
//        //System.out.println("Sum :"+sum);
//        //System.out.println("Length of array :"+squarederror.length);
//        // Finding mean
//        double mean = sum/squarederror.length;
//        System.out.println("MSE : " + p.mse);

        double threshold = 0.038;
        int anomaly = 0;
        // Add the errors


        // Finding Mean
        if(p.mse > threshold){
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(dp);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormat.parse(dp.getStatTime());
            long timestamp = date.getTime()/1000L;
            rt.opsForZSet().add(dp.getName() + "-Anomalies", json, timestamp);
            System.out.println("Saved Anomalies");
            anomaly = 1;
        }

        return anomaly;
    }


//-----------------------------------------------------------------------------------
//------------------------------FUNCTIONS FOR VIEW-----------------------------------
// ----------------------------------------------------------------------------------
//    @Override
//    public void save_data(DataPoint dp) throws ParseException, JsonProcessingException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = dateFormat.parse(dp.getStatTime());
//        long timestamp = date.getTime()/1000L;
//
//        String json1 = objectMapper.writeValueAsString(dp);
//        rt.opsForZSet().add(dp.getName(), json1, timestamp);
//    }
//
//    @Override
//    public void save_anomalies(DataPoint dp) throws ParseException, JsonProcessingException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = dateFormat.parse(dp.getStatTime());
//        long timestamp = date.getTime()/1000L;
//
//        String json1 = objectMapper.writeValueAsString(dp);
//        rt.opsForZSet().add(dp.getName()+"-Anomalies", json1, timestamp);
//    }
//

    @Override
    public int get_total_datapoints_today(){
        // Get Current timestamp in unix format
        long curr_timestamp = System.currentTimeMillis() / 1000L;

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long min_timestamp = cal.getTimeInMillis()/ 1000L;

        // Fetching datapoints of each terminal
        Set<String> keys_all = rt.keys("TERM?????");
        int[] num_datapoints = new int[keys_all.size()];
        int[] datapointsperhour = new int[24];
        Iterator<String> d_it = keys_all.iterator();
        int i = 0;

        // Fetching all datapoints
        while(d_it.hasNext()){
            String key_name = d_it.next();
            Set<Object> data = rt.opsForZSet().rangeByScore(key_name, min_timestamp, curr_timestamp);
            num_datapoints[i] = data.size();
            i = i+1;
        }
        int total_datapoints = IntStream.of(num_datapoints).sum();

        return total_datapoints;
    }


    @Override
    public int get_total_anomalies_today(){
        // Get Current timestamp in unix format
        long curr_timestamp = System.currentTimeMillis() / 1000L;

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long min_timestamp = cal.getTimeInMillis()/ 1000L;

        // Fetching datapoints of each terminal
        Set<String> keys_all = rt.keys("?????????-Anomalies");
        int[] num_anomalies = new int[keys_all.size()];
        Iterator<String> d_it = keys_all.iterator();
        int i = 0;

        // Fetching all datapoints
        while(d_it.hasNext()){
            String key_name = d_it.next();
            Set<Object> data = rt.opsForZSet().rangeByScore(key_name, min_timestamp, curr_timestamp);
            num_anomalies[i] = data.size();
            i = i+1;
        }
        int total_datapoints = IntStream.of(num_anomalies).sum();

        return total_datapoints;
    }


    @Override
    public List<DataPoint> get_listof_todays_anomalies() throws JsonProcessingException {
        List<DataPoint> list= new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        // Get Current timestamp in unix format
        long curr_timestamp = System.currentTimeMillis() / 1000L;

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long min_timestamp = cal.getTimeInMillis()/ 1000L;


        // Fetching datapoints of each terminal
        Set<String> keys_all = rt.keys("?????????-Anomalies");
        Iterator<String> iter = keys_all.iterator();

        while(iter.hasNext()){
            String key_name = iter.next();
            Set<Object> anomalies = rt.opsForZSet().rangeByScore(key_name, min_timestamp, curr_timestamp);

            Iterator<Object> iter2 = anomalies.iterator();
            while ((iter2.hasNext())){
                list.add(mapper.readValue((String) iter2.next(), DataPoint.class));
            }
        }

        return list;
    }

    @Override
    public int get_total_terminals(){
        Set<String> keys_all = rt.keys("TERM?????");
        return keys_all.size();
    }


    @Override
    public int[] get_per_hour_anomalies(){
        int[] anomaliesperhour = new int[24];

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        int current_hour = cal.get(Calendar.HOUR_OF_DAY);

        Set<String> keys_all = rt.keys("?????????-Anomalies");


        for(int k=0; k<=current_hour; k++){
            Iterator<String> d_it = keys_all.iterator();
            Calendar r1 = Calendar.getInstance();
            r1.setTimeInMillis(System.currentTimeMillis());
            r1.set(Calendar.HOUR_OF_DAY, k);
            r1.set(Calendar.MINUTE, 0);
            r1.set(Calendar.SECOND, 0);
            r1.set(Calendar.MILLISECOND, 0);
            long range1 = r1.getTimeInMillis()/ 1000L;

            Calendar r2 = Calendar.getInstance();
            r2.setTimeInMillis(System.currentTimeMillis());
            r2.set(Calendar.HOUR_OF_DAY, k);
            r2.set(Calendar.MINUTE, 59);
            r2.set(Calendar.SECOND, 59);
            r2.set(Calendar.MILLISECOND, 999);
            long range2 = r2.getTimeInMillis()/ 1000L;

            int[] perhouranomaly = new int[keys_all.size()];
            int perhouranomaly_index = 0;

            while(d_it.hasNext()){
                String key_name = d_it.next();
                Set<Object> data = rt.opsForZSet().rangeByScore(key_name, range1, range2);
                perhouranomaly[perhouranomaly_index] = data.size();
                perhouranomaly_index = perhouranomaly_index + 1;

            }
            anomaliesperhour[k] = IntStream.of(perhouranomaly).sum();
        }

        return anomaliesperhour;
    }


    @Override
    public int[] get_per_hour_datapoints(){
        int[] datapointsperhour = new int[24];

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        int current_hour = cal.get(Calendar.HOUR_OF_DAY);

        Set<String> keys_all = rt.keys("TERM?????");

        for(int k=0; k<=current_hour; k++){
            Iterator<String> d_it = keys_all.iterator();
            Calendar r1 = Calendar.getInstance();
            r1.setTimeInMillis(System.currentTimeMillis());
            r1.set(Calendar.HOUR_OF_DAY, k);
            r1.set(Calendar.MINUTE, 0);
            r1.set(Calendar.SECOND, 0);
            r1.set(Calendar.MILLISECOND, 0);
            long range1 = r1.getTimeInMillis()/ 1000L;

            Calendar r2 = Calendar.getInstance();
            r2.setTimeInMillis(System.currentTimeMillis());
            r2.set(Calendar.HOUR_OF_DAY, k);
            r2.set(Calendar.MINUTE, 59);
            r2.set(Calendar.SECOND, 59);
            r2.set(Calendar.MILLISECOND, 999);
            long range2 = r2.getTimeInMillis()/ 1000L;

            int[] perhourdatapoint = new int[keys_all.size()];
            int perhourdatapoint_index = 0;

            while(d_it.hasNext()){
                String key_name = d_it.next();
                Set<Object> data = rt.opsForZSet().rangeByScore(key_name, range1, range2);
                perhourdatapoint[perhourdatapoint_index] = data.size();
                perhourdatapoint_index = perhourdatapoint_index + 1;

            }
            datapointsperhour[k] = IntStream.of(perhourdatapoint).sum();
        }

        return datapointsperhour;
    }


    // Get list of anomalies
    @Override
    public List<DataPoint> get_list_of_anomalies(Inputs inputs) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        DataPoint dp = new DataPoint();
        List<DataPoint> list = new ArrayList<>();

        // Create timestamp for range-1
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.DATE, inputs.getTimestamp1_date());
        cal1.set(Calendar.MONTH, inputs.getTimestamp1_month());
        cal1.set(Calendar.YEAR, inputs.getTimestamp1_year());
        cal1.set(Calendar.HOUR_OF_DAY, inputs.getTimestamp1_hour());
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        long min_timestamp = cal1.getTimeInMillis()/ 1000L;

        // Create timestamp for range-2
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(System.currentTimeMillis());
        cal2.set(Calendar.DATE, inputs.getTimestamp2_date());
        cal2.set(Calendar.MONTH, inputs.getTimestamp2_month());
        cal2.set(Calendar.YEAR, inputs.getTimestamp2_year());
        cal2.set(Calendar.HOUR_OF_DAY, inputs.getTimestamp2_hour());
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        long max_timestamp = cal2.getTimeInMillis()/ 1000L;

//      Check Terminal Name
        if("All".equals(inputs.getTerminal_name())){
            // Fetching datapoints of each terminal
            Set<String> keys_all = rt.keys("?????????-Anomalies");
            Iterator<String> iter = keys_all.iterator();

            while(iter.hasNext()){
                String key_name = iter.next();
                Set<Object> anomalies = rt.opsForZSet().rangeByScore(key_name, min_timestamp, max_timestamp);

                Iterator<Object> iter2 = anomalies.iterator();
                while ((iter2.hasNext())){
                    list.add(mapper.readValue((String) iter2.next(), DataPoint.class));
                }
            }
        }
        else {
            Set<Object> anomalies = rt.opsForZSet().rangeByScore(inputs.getTerminal_name()+"-Anomalies", min_timestamp, max_timestamp);
            Iterator<Object> iter = anomalies.iterator();
            while ((iter.hasNext())){
                list.add(mapper.readValue((String) iter.next(), DataPoint.class));
            }
        }

        System.out.println("Size: " + list.size());
        return list;
    }

    @Override
    public int get_total_datapoints(Inputs inputs) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        DataPoint dp = new DataPoint();
        List<DataPoint> list = new ArrayList<>();

        // Create timestamp for range-1
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.DATE, inputs.getTimestamp1_date());
        cal1.set(Calendar.MONTH, inputs.getTimestamp1_month());
        cal1.set(Calendar.YEAR, inputs.getTimestamp1_year());
        cal1.set(Calendar.HOUR_OF_DAY, inputs.getTimestamp1_hour());
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        long min_timestamp = cal1.getTimeInMillis()/ 1000L;

        // Create timestamp for range-2
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(System.currentTimeMillis());
        cal2.set(Calendar.DATE, inputs.getTimestamp2_date());
        cal2.set(Calendar.MONTH, inputs.getTimestamp2_month());
        cal2.set(Calendar.YEAR, inputs.getTimestamp2_year());
        cal2.set(Calendar.HOUR_OF_DAY, inputs.getTimestamp2_hour());
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        long max_timestamp = cal2.getTimeInMillis()/ 1000L;

//      Check Terminal Name
        if("All".equals(inputs.getTerminal_name())){
            // Fetching datapoints of each terminal
            Set<String> keys_all = rt.keys("TERM?????");
            Iterator<String> iter = keys_all.iterator();

            while(iter.hasNext()){
                String key_name = iter.next();
                Set<Object> anomalies = rt.opsForZSet().rangeByScore(key_name, min_timestamp, max_timestamp);

                Iterator<Object> iter2 = anomalies.iterator();
                while ((iter2.hasNext())){
                    list.add(mapper.readValue((String) iter2.next(), DataPoint.class));
                }
            }
        }
        else {
            Set<Object> anomalies = rt.opsForZSet().rangeByScore(inputs.getTerminal_name(), min_timestamp, max_timestamp);
            Iterator<Object> iter = anomalies.iterator();
            while ((iter.hasNext())){
                list.add(mapper.readValue((String) iter.next(), DataPoint.class));
            }
        }

        return list.size();
    }

    @Override
    public List<line_graph_data> get_hourly_linegraph_anomalies(Inputs inputs) throws JsonProcessingException {

        List<line_graph_data> anomaliesperhour_list = new ArrayList<>();

//------Conditions to check number of days
            Set<String> keys_all = rt.keys("?????????-Anomalies");

            for(int k=inputs.getTimestamp1_hour(); k<=inputs.getTimestamp2_hour(); k++){

                line_graph_data lgd = new line_graph_data();

                Iterator<String> d_it = keys_all.iterator();
                Calendar r1 = Calendar.getInstance();
                r1.set(Calendar.DATE, inputs.getTimestamp1_date());
                r1.set(Calendar.MONTH, inputs.getTimestamp1_month());
                r1.set(Calendar.YEAR, inputs.getTimestamp1_year());
                r1.set(Calendar.HOUR_OF_DAY, k);
                r1.set(Calendar.MINUTE, 0);
                r1.set(Calendar.SECOND, 0);
                r1.set(Calendar.MILLISECOND, 0);
                long range1 = r1.getTimeInMillis()/ 1000L;

                Calendar r2 = Calendar.getInstance();
                r2.set(Calendar.DATE, inputs.getTimestamp1_date());
                r2.set(Calendar.MONTH, inputs.getTimestamp1_month());
                r2.set(Calendar.YEAR, inputs.getTimestamp1_year());
                r2.set(Calendar.HOUR_OF_DAY, k);
                r2.set(Calendar.MINUTE, 59);
                r2.set(Calendar.SECOND, 59);
                r2.set(Calendar.MILLISECOND, 999);
                long range2 = r2.getTimeInMillis()/ 1000L;

                if("All".equals(inputs.getTerminal_name())) {
                    int[] onehouranomaly = new int[keys_all.size()];
                    int onehouranomaly_index = 0;

                    while (d_it.hasNext()) {
                        String key_name = d_it.next();
                        Set<Object> data = rt.opsForZSet().rangeByScore(key_name, range1, range2);
                        onehouranomaly[onehouranomaly_index] = data.size();
                        onehouranomaly_index = onehouranomaly_index + 1;

                    }
                    lgd.setNum_anomalies(IntStream.of(onehouranomaly).sum());
                }

                else{
                    Set<Object> anomalies = rt.opsForZSet().rangeByScore(inputs.getTerminal_name()+"-Anomalies", range1, range2);
                    lgd.setNum_anomalies(anomalies.size());
                }
                lgd.setHour(k);
                anomaliesperhour_list.add(lgd);
            }

            return anomaliesperhour_list;
    }

    @Override
    public List<line_graph_data> get_daily_linegraph_anomalies(Inputs inputs) throws JsonProcessingException {

        List<line_graph_data> daily_anomalies_list = new ArrayList<>();

//------Conditions to check number of days

        Calendar start_date = Calendar.getInstance();
        start_date.set(Calendar.DATE, inputs.getTimestamp1_date());
        start_date.set(Calendar.MONTH, inputs.getTimestamp1_month());
        start_date.set(Calendar.YEAR, inputs.getTimestamp1_year());
        start_date.set(Calendar.HOUR_OF_DAY, 0);
        start_date.set(Calendar.MINUTE, 0);
        start_date.set(Calendar.SECOND, 0);
        start_date.set(Calendar.MILLISECOND, 0);


        while (start_date.get(Calendar.DATE) != inputs.getTimestamp2_date() ||
                start_date.get(Calendar.MONTH) != inputs.getTimestamp2_month() ||
                start_date.get(Calendar.YEAR) != inputs.getTimestamp2_year()){

            line_graph_data lgd = new line_graph_data();
            Set<String> keys_all = rt.keys("?????????-Anomalies");
            Iterator<String> d_it = keys_all.iterator();
            long range1 = start_date.getTimeInMillis()/ 1000L;

            Calendar date_range_2 = Calendar.getInstance();
            date_range_2.set(Calendar.DATE, start_date.get(Calendar.DATE));
            date_range_2.set(Calendar.MONTH, start_date.get(Calendar.MONTH));
            date_range_2.set(Calendar.YEAR, start_date.get(Calendar.YEAR));
            date_range_2.set(Calendar.HOUR_OF_DAY, 23);
            date_range_2.set(Calendar.MINUTE, 59);
            date_range_2.set(Calendar.SECOND,59);
            date_range_2.set(Calendar.MILLISECOND, 999);
            long range2 = date_range_2.getTimeInMillis()/ 1000L;

            if("All".equals(inputs.getTerminal_name())) {
                int[] onedayanomaly = new int[keys_all.size()];
                int onedayanomaly_index = 0;

                while (d_it.hasNext()) {
                    String key_name = d_it.next();
                    Set<Object> data = rt.opsForZSet().rangeByScore(key_name, range1, range2);
                    onedayanomaly[onedayanomaly_index] = data.size();
                    onedayanomaly_index = onedayanomaly_index + 1;
                }

                lgd.setNum_anomalies(IntStream.of(onedayanomaly).sum());

            }

            else {

                Set<Object> anomalies = rt.opsForZSet().rangeByScore(inputs.getTerminal_name()+"-Anomalies", range1, range2);
                lgd.setNum_anomalies(anomalies.size());

            }

            lgd.setDate(start_date.get(Calendar.DATE));
            lgd.setMonth(start_date.get(Calendar.MONTH));
            lgd.setYear(start_date.get(Calendar.YEAR));
            daily_anomalies_list.add(lgd);
            start_date.add(Calendar.DAY_OF_MONTH, 1);

        }

        return daily_anomalies_list;
    }

}
