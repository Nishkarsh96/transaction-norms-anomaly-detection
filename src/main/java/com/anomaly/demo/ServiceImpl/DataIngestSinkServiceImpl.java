package com.anomaly.demo.ServiceImpl;

import com.anomaly.demo.DataBinding;
import com.anomaly.demo.Entity.DataPoint;
import com.anomaly.demo.Entity.PreprocessedPoint;
import com.anomaly.demo.Service.DataIngestSinkService;
import com.anomaly.demo.Service.RedisIngestService;
import com.anomaly.demo.Service.TerminalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


@Component
@EnableBinding(DataBinding.class)
public class DataIngestSinkServiceImpl implements DataIngestSinkService {

    @Autowired
    RedisIngestService redis;

    @Autowired
    RedisTemplate<String,Object> rt;

    @Autowired
    TerminalService ts;

    private final Log log = LogFactory.getLog(getClass());
    private final MessageChannel dataOut;

    public DataIngestSinkServiceImpl(DataBinding binding) {
        this.dataOut = binding.dataOut();
    }

    public DataPoint createData() throws JsonProcessingException {
        DataPoint dataPoint = new DataPoint();

        Set<String> keys_all = rt.keys("TERM?????");
        String[] names = new String[keys_all.size()];
        Iterator<String> iter = keys_all.iterator();
        int i = 0;
        // Fetching all terminal names
        while(iter.hasNext()){
            String key_name = iter.next();
            names[i] = key_name;
            i = i+1;
        }

        String sel_key = names[new Random().nextInt(100)];
        Set<Object> data = rt.opsForZSet().range(sel_key, 0, -1);
        int num_of_values = data.size();
        int sel_value = new Random().nextInt(num_of_values);
        ObjectMapper mapper = new ObjectMapper();
        Iterator jsonIter = data.iterator();
        i=0;
        while (jsonIter.hasNext() && i<=sel_value) {
            dataPoint = mapper.readValue((String) jsonIter.next(), DataPoint.class);
            i = i+1;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
        format1.setTimeZone(TimeZone.getTimeZone("IST"));
        String formatted = format1.format(cal.getTime());
        dataPoint.setStatTime(formatted);
        System.out.println("Returning Generated Data");
        return dataPoint;
    }

    public void stream() {
        Runnable runnable = () -> {
            DataPoint d = new DataPoint();
            try {
                d = createData();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Message<DataPoint> message = MessageBuilder
                    .withPayload(d)
                    .setHeader(d.getName(), d)
                    .build();
            try {
                this.dataOut.send(message);
                log.info("sent this:" + message.toString());
            }
            catch (Exception e) {
                log.error(e);
            }
        };

        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(runnable, 100, 100, TimeUnit.MILLISECONDS);
    }

    @StreamListener
    public void process(@Input(DataBinding.DATA_IN) KStream<String, DataPoint> dataPoint) {
        dataPoint
                .map((key, value) -> new KeyValue<>(value.getStatTime(), value))
                .foreach((key, datapoint) -> {
                    try {
                        redis.Ingest(key, datapoint);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        System.out.println(ts.PredictResults(ts.PreprocessData(datapoint), datapoint));
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                });
    }
}
