package com.anomaly.demo.ServiceImpl;

import com.anomaly.demo.Entity.DataPoint;
import com.anomaly.demo.Service.RedisIngestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class RedisIngestServiceImpl implements RedisIngestService {

    @Autowired
    RedisTemplate<String,Object> rt;

    private final Log log = LogFactory.getLog(getClass());

    @Override
    public void Ingest(String i, DataPoint dataPoint) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(dataPoint);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormat.parse(dataPoint.getStatTime());
            long timestamp = date.getTime()/1000L;

            rt.opsForZSet().add(dataPoint.getName(), json, timestamp);
            log.info("Data ingested successfully");
        }
        catch (Exception e) {
            e.printStackTrace();
            log.error("Error ingesting!!! " + e);
        }
    }

    @Override
    public String IngestList(List<DataPoint> dataPointList) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            for (DataPoint restDataPoint : dataPointList) {
                String json = objectMapper.writeValueAsString(restDataPoint);

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = dateFormat.parse(restDataPoint.getStatTime());
                long timestamp = date.getTime()/1000L;

                rt.opsForZSet().add(restDataPoint.getName(),
                        json, timestamp);
                return "Successfully ingested";
            }
        } catch (JsonProcessingException | ParseException e) {
            e.printStackTrace();
        }
        return "There was an error ingesting!!!";
    }
}
