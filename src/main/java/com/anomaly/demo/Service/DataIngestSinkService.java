package com.anomaly.demo.Service;

import com.anomaly.demo.DataBinding;
import com.anomaly.demo.Entity.DataPoint;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.stereotype.Service;

@Service
public interface DataIngestSinkService {

    DataPoint createData() throws JsonProcessingException;

    void stream();

    void process(@Input(DataBinding.DATA_IN) KStream<String, DataPoint> dataPoint);
}
