package com.anomaly.demo.Service;

import com.anomaly.demo.Entity.DataPoint;
import com.anomaly.demo.Entity.Inputs;
import com.anomaly.demo.Entity.PreprocessedPoint;
import com.anomaly.demo.Entity.line_graph_data;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.util.List;

@Service
public
interface TerminalService {

     PreprocessedPoint PreprocessData(DataPoint dp) throws ParseException;

     int PredictResults(PreprocessedPoint lpp, DataPoint dp) throws Exception;

//-----------------------------------------------------------------------------------
//------------------------------Functions for view-----------------------------------
// ----------------------------------------------------------------------------------
     int get_total_anomalies_today();

     int get_total_datapoints_today();

     int get_total_terminals();

     int[] get_per_hour_anomalies();

     int[] get_per_hour_datapoints();

     List<DataPoint> get_list_of_anomalies(Inputs inputs) throws JsonProcessingException;

     int get_total_datapoints(Inputs inputs) throws JsonProcessingException;

     List<DataPoint> get_listof_todays_anomalies() throws JsonProcessingException;

     List<line_graph_data> get_hourly_linegraph_anomalies(Inputs inputs) throws JsonProcessingException;

     List<line_graph_data> get_daily_linegraph_anomalies(Inputs inputs) throws JsonProcessingException;


}
