package com.anomaly.demo.Service;

import com.anomaly.demo.Entity.DataPoint;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RedisIngestService {

    void Ingest(String i, DataPoint dataPoint) throws Exception;

    String IngestList(List<DataPoint> dataPointList);
}
