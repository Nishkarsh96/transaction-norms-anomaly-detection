package com.anomaly.demo.Controller;

import com.anomaly.demo.Entity.*;
import com.anomaly.demo.Service.TerminalService;
import com.anomaly.demo.ServiceImpl.DataIngestSinkServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/")
public class TerminalController {

    @Autowired
    RedisTemplate<String,Object> rt;

    @Autowired
    TerminalService ts;

    @Autowired
    DataIngestSinkServiceImpl ingest;

    @RequestMapping("predict")
    public void Stream() {
        ingest.stream();
    }

    private final Log log = LogFactory.getLog(getClass());

    //---------- Get all terminals fixed data----------------------------------------------------------------------------
    @PostMapping("saveDummyData")
    public String saveDummyData(@RequestBody List<DataPoint> datapoints) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        for (int i = 0; i < datapoints.size(); i++) {
            String json = objectMapper.writeValueAsString(datapoints.get(i));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormat.parse(datapoints.get(i).getStatTime());
            long timestamp = date.getTime() / 1000L;

            int k = ts.PredictResults(ts.PreprocessData(datapoints.get(i)), datapoints.get(i));

            rt.opsForZSet().add(datapoints.get(i).getName(), json, timestamp);
            }
        return "Saved Successfully";
    }


    //---------- Get all terminals fixed data--------------------------------------------------------
    @GetMapping("getfixeddata")
    public fixedData getfixeddata() throws ParseException, JsonProcessingException {
        fixedData fD = new fixedData();
        fD.setTotalterminals(ts.get_total_terminals());
        fD.setTotalnum_anomaliestoday(ts.get_total_anomalies_today());
        fD.setTotalnum_datapointstoday(ts.get_total_datapoints_today());
        fD.setTodayperhouranomalies(ts.get_per_hour_anomalies());
        fD.setTodayperhourdatapoints(ts.get_per_hour_datapoints());
        fD.setListofanomaliestoday(ts.get_listof_todays_anomalies());
        return fD;
    }

    //---------- Get all terminal names---------------------------------------------------------------
    @GetMapping("getterminalnames")
    public String[] getterminalnames() throws ParseException, JsonProcessingException {
        Set<String> keys_all = rt.keys("TERM?????");
        String[] names = new String[keys_all.size()];
        Iterator<String> iter = keys_all.iterator();
        int i = 0;
        // Fetching all datapoints
        while(iter.hasNext()){
            String key_name = iter.next();
            names[i] = key_name;
            i = i+1;
        }
        return names;
    }

    //---------- Get List of anomalies---------------------------------------------------------------
    @PostMapping(value ="getlistofanomalies")
    public ListOfAnomaliesData getlistofanomalies(@RequestBody Inputs inputs) throws ParseException, JsonProcessingException {
        ListOfAnomaliesData load = new ListOfAnomaliesData();

        List<DataPoint> lst = new ArrayList<>();
//        List<Integer> linegraph_datapoints = new ArrayList<>();
        List<line_graph_data> linegraph_anomalies =new ArrayList<>();
        int total_anomalies;
        int total_datapoints;
        int normal_datapoints;

        lst = ts.get_list_of_anomalies(inputs);
        load.setLst(lst);

        total_anomalies = lst.size();
        load.setTotal_anomalies(total_anomalies);

//        linegraph_datapoints = ts.get_linegraph_datapoints(inputs);
//        load.setLinegraph_datapoints(linegraph_datapoints);

//          If true return hourly data
            if(inputs.getTimestamp1_date() == inputs.getTimestamp2_date() &&
                    inputs.getTimestamp1_month() == inputs.getTimestamp2_month() &&
                    inputs.getTimestamp1_year() == inputs.getTimestamp2_year()) {

                linegraph_anomalies = ts.get_hourly_linegraph_anomalies(inputs);
                load.setData_type("hourly");
                load.setLinegraph_anomalies(linegraph_anomalies);

                System.out.println("Hourly Data Size");
                System.out.println(linegraph_anomalies.size());

            }
//      Else return daily day
            else {

                linegraph_anomalies = ts.get_daily_linegraph_anomalies(inputs);
                load.setData_type("daily");
                load.setLinegraph_anomalies(linegraph_anomalies);

                System.out.println("Daily Data Size");
                System.out.println(linegraph_anomalies.size());

            }

        total_datapoints = ts.get_total_datapoints(inputs);
        load.setTotal_datapoints(total_datapoints);

        normal_datapoints = total_datapoints - lst.size();
        load.setTotal_normal_datapoints(normal_datapoints);

        return load;
    }


}
