package com.anomaly.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping(value = "/home")
    public String home() {
        return "home";
    }

    @GetMapping(value = "/check")
    public String check() {
        return "check";
    }

}
