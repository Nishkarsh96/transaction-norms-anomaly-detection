// Start Stream
function start_datastream(){

    for(var i=0; i<10000; i++){

//      // Calling stream function
//      setTimeout(function(){
//                $.ajax({url: "start", type: "POST", success: function(result){
//                      console.log(result)
//                       }});
//                       }, 2000 * i);

    //  Call view update function
    setTimeout(function(){
            console.log("1");
            //--- Get all fixed data------------------------------------------------------------------------------------------------
                 $.ajax({url: "getfixeddata", success: function(result){
                    $('#totalanomaliestodayn').empty();
                    $('#totalanomaliestodayn').append(result.totalnum_anomaliestoday);
                    $('#totalterminalsn').empty();
                    $('#totalterminalsn').append(result.totalterminals);
                    $('#totaldatapointstodayn').empty();
                    $('#totaldatapointstodayn').append(result.totalnum_datapointstoday);
                    var arr_anomalies = result.todayperhouranomalies;
                    var arr_datapoints = result.todayperhourdatapoints;

                    var todaysanomalies = result.listofanomaliestoday;
                    $('#listoftodaysanomalies').empty();
                    for(i=0; i<todaysanomalies.length; i++){
                        var li = "<li>" + todaysanomalies[i].Name + " | " +todaysanomalies[i].StatTime + " | " +todaysanomalies[i].Total+ " | " +todaysanomalies[i].RespTime + "</li>" ;
                        $('#listoftodaysanomalies').append(li);
                    }

            console.log("2");
            //--- Draw Main Chart-------------------------------------------------
                google.load("visualization", "1", {packages:["corechart"]});
                google.charts.setOnLoadCallback(mainchart);

                function mainchart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Hour');
                data.addColumn('number', 'Datapoints');
                data.addColumn('number', 'Anomalies');

                for(i=0; i<arr_datapoints.length; i++){
                console.log(arr_datapoints[i]);
                    data.addRows([
                    [i.toString(), parseInt(arr_datapoints[i]), parseInt(arr_anomalies[i])]
                    ]);
                    }

                var options = {
                    backgroundColor: "white",
                    title: 'Datapoints v/s Anomalies',
                    hAxis: {title: 'Hours',showTextEvery: 1, titleTextStyle: {
                                                                 color: 'black'
                                                               }},
                    vAxes: {0: {viewWindowMode:'explicit',
                                title: 'Datapoints',
                                gridlines: {color: 'grey'},
                                titleTextStyle: {
                                 color: 'black'
                                   }
                                },
                            1: {
                                title: 'Anomalies',
                                gridlines: {color: 'grey'},
                                titleTextStyle: {
                                     color: 'black'
                                   }
                                },
                    },
                    series: {0: {targetAxisIndex:0},
                            1:{targetAxisIndex:1},
                    },
                    colors: ["green", "red"],
//                    chartArea: {left:60,top:30,width:'80%',height:'70%'},
                };

                var chart = new google.visualization.LineChart(document.getElementById('mainchart'));
                chart.draw(data, options);
            }

            }});
//----------------------------------------------------------------------------------------------------------------------

        console.log("6");
        }, 500 * i);

//        setTimeout(function(){ console.log(">>Updated<<"); }, 5000);

    console.log(i);
}

}
