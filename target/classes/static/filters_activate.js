function terms_changed(termsCheckBox){
    //If the checkbox has been checked
    if(termsCheckBox.checked){
        //Set the disabled property to FALSE and enable the button.
        document.getElementById("selectterm_b").disabled = false;

        document.getElementById("selectdate_b1").disabled = false;
        document.getElementById("selectmonth_b1").disabled = false;
        document.getElementById("selectyear_b1").disabled = false;
        document.getElementById("selecthour_b1").disabled = false;

        document.getElementById("selectdate_b2").disabled = false;
        document.getElementById("selectmonth_b2").disabled = false;
        document.getElementById("selectyear_b2").disabled = false;
        document.getElementById("selecthour_b2").disabled = false;
    } else{
        //Otherwise, disable the submit button.
        document.getElementById("selectterm_b").disabled = true;

        document.getElementById("selectdate_b1").disabled = true;
        document.getElementById("selectmonth_b1").disabled = true;
        document.getElementById("selectyear_b1").disabled = true;
        document.getElementById("selecthour_b1").disabled = true;

        document.getElementById("selectdate_b2").disabled = true;
        document.getElementById("selectmonth_b2").disabled = true;
        document.getElementById("selectyear_b2").disabled = true;
        document.getElementById("selecthour_b2").disabled = true;
    }
}